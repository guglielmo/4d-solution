# 4d-solution

This solution shows how to deploy a django app to a docker swarm using gitlab pipelines and exploiting Portainer’s webhooks feature.
The rationale behind this solution is [described in this post](https://lab.openpolis.io/posts/django_deployment_with_docker_for_dinosaurs/)

## Prerequisites

- a gitlab instance
- a runner registered and enabled for the project

## Forking the project
You should be able to use http://gitlab.com,
after having forked the project, just
```
git clone PROJECT_URL
```
to a local directory.

## Test locally (no docker)
Create a virtualenv and install all required packages
```bash
python -m venv venv
source venv/bin/activate
pip install -r backend/reuirements.txt
```

Setup a database, locally, or remotely, and annotate the connection parameters.
Setup an AWS bucket on S3, or a DigitalOcean Block, and annotate the values.
Copy ``.env.sample`` into ``.env``, substitute the values for database and storage
connections, then:

```bash
export $(grep -v '^#' .env | xargs)
python backend/manage.py migrate
python backend/manage.py createsuperuser
python backend/manage.py collectstatic
python backend/manage.py runserver
```

Visit: http://localhost:8000/admin
You should be presented with the login mask to the django admin site,
with static files taken from the remote storage. A user (the superuser) should
be present.

Visit: http://localhost:8000/api/health-check/.
You should get an OK response, in JSON.

If you're a ``tmuxinator`` fan, like me, you'll find a  ``.tmuxinator.yml`` file
that could be helpful.

## Build the services images
The registry should be enabled on the gitlab instance, then,
given that the registry url is CI_REGISTRY, and CI_REGISTRY_USER and
CI_REGISTRY_PASSWORD grant access to your gitlab instance:
```bash
docker login -u ${CI_REGISTRY_USER} -p ${CI_REGISTRY_PASSWORD} ${CI_REGISTRY}
docker build -t ${CI_REGISTRY}/nginx-web:latest -f nginx/prod/Dockerfile .
docker push ${CI_REGISTRY}/nginx-web:latest
docker build -f backend/docker/Dockerfile.prod -t ${CI_REGISTRY}/backend:latest ./backend/
docker push ${CI_REGISTRY}/backend:latest
```

## Create a stack with portainer
A docker swarm should be already running somewhere, and [Portainer](https:://portainer.io)
must already have been installed on it. [TODO]

- create an external database (managed or self-hosted)
- create a managed storage (S3, Digital Ocean)
- go to the stacks section, add a stack and copy the content of
  ``docker-compose.yml``, taking care of the environment variables, that must be
  created in the editor sections and referenced in the stack definition.
  Expand the registry URLS for the images, using full URLs, without
  ``$CI_*`` variables.

See that the services are correctly created.

Test to increase the number of replicas for the ``backend`` service.

Define webhooks, and add their URL to the env variable in gitlab.

## Push the project and test code changes

- define all needed environment variables in Gitlab, with connection to DB/S3
- define a DNS record for the domain used (Route53)
- commit+push the source code.

See that the services are correctly updated and everything works correctly on the remote

## Things to try:

- Can you have 5 backend services and 2 nginx? Why?
- Try to remove completely the need for nginx, using uwsgi http.
- Add another API route, push the code, and see that all services are upgraded.
