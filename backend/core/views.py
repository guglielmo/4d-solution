from django.http import JsonResponse


# Create your views here.
def health_check(request):
    response = JsonResponse({"message": "OK"})
    return response
